package ru.narod.nod.dawanda.detailed_mvp;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;
import java.util.Map;

import ru.narod.nod.dawanda.Model.ModelProductListOfCategory.ModelProductListOfCategory;
import ru.narod.nod.dawanda.ReciclerViewPack.RVAdapter_ModelProductListOfCategory;

interface DetailedView
        extends MvpView {

    void showInfo(Map itemInfo, Drawable drawable);
}
