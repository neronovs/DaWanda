package ru.narod.nod.dawanda.ReciclerViewPack;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.narod.nod.dawanda.Model.ModelProductListOfCategory.ModelProductListOfCategory;
import ru.narod.nod.dawanda.R;
import ru.narod.nod.dawanda.databinding.MiddleItemViewBinding;

public class RVAdapter_ModelProductListOfCategory
        extends RecyclerView.Adapter<RVAdapter_ModelProductListOfCategory.ItemViewHolder> {

    private List<ModelProductListOfCategory> model;
    private final String TAG = "myLogs." + getClass().getSimpleName();
    private Context context;
    private MiddleItemViewBinding middleItemViewBinding;

    public RVAdapter_ModelProductListOfCategory(
            Context context, List<ModelProductListOfCategory> model) {
        this.model = model;
        this.context = context;
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {
        ModelProductListOfCategory modelProductListOfCategory;

        TextView middle_tvTitle;
        TextView middle_tvPrice;
        ImageView middle_defaultImage;
        com.wang.avi.AVLoadingIndicatorView progressBar;
        private final String TAG = "myLogs." + getClass().getSimpleName();

        ItemViewHolder(final View itemView) {
            super(itemView);
            middleItemViewBinding = DataBindingUtil.bind(itemView);

            middle_tvTitle = middleItemViewBinding.middleTvTitle;
            middle_tvPrice = middleItemViewBinding.middleTvPrice;
            middle_defaultImage = middleItemViewBinding.middleImage;
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.middle_item_view, viewGroup, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder itemViewHolder, final int position) {
        itemViewHolder.modelProductListOfCategory = model.get(position);

        //Getting a title
        itemViewHolder.middle_tvTitle.setText(model.get(position).getTitle());

        //Price info fetching (Cents are dividing by 100 to get, f.i., Euros)
        double price = Double.parseDouble(
                model.get(position).getPrice().getCents().toString()) / 100;
        String priceWithCurrency = model.get(position).getPrice().getCurrency()
                .concat(" ")
                .concat(String.valueOf(price));
        itemViewHolder.middle_tvPrice.setText(priceWithCurrency);

        //Loading image ("default_image['product_l']") asynchronously using the Picasso library
        String pic_url = "http:" + model.get(position).getDefaultImage().getProductL();
        if (!pic_url.equals("")) {
            Picasso.with(context)
                    .load(pic_url)
                    .into(itemViewHolder.middle_defaultImage);
        } else {
            Log.i("Url 4 Picasso is empty!", String.valueOf(model.get(position).getId()));
        }
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}