
package ru.narod.nod.dawanda.Model.ModelProductListOfCategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UndiscountedPrice {

    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("cents")
    @Expose
    private Integer cents;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getCents() {
        return cents;
    }

    public void setCents(Integer cents) {
        this.cents = cents;
    }

}
