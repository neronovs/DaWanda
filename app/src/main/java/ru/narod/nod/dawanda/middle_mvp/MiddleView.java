package ru.narod.nod.dawanda.middle_mvp;

import android.support.v7.widget.RecyclerView;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import ru.narod.nod.dawanda.Model.ModelProductListOfCategory.ModelProductListOfCategory;
import ru.narod.nod.dawanda.ReciclerViewPack.RVAdapter_ModelProductListOfCategory;

public interface MiddleView
        extends MvpView {

    void startProgressBar();
    void stopProgressBar();
    void showInfo(List<ModelProductListOfCategory> modelCategoryArrayList);
    RecyclerView getReciclerView();
    RVAdapter_ModelProductListOfCategory getAdapter();
}
