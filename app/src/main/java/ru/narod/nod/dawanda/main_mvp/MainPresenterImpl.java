package ru.narod.nod.dawanda.main_mvp;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.io.IOException;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.narod.nod.dawanda.Model.DataKeeper;
import ru.narod.nod.dawanda.Model.ModelCategory;
import ru.narod.nod.dawanda.R;
import ru.narod.nod.dawanda.RestManager.JSonParser;
import ru.narod.nod.dawanda.RestManager.RestfulManager;
import ru.narod.nod.dawanda.RestManager.RestfulManagerCategories;
import ru.narod.nod.dawanda.middle_mvp.MiddleActivity;


class MainPresenterImpl
        extends MvpBasePresenter<MainView>
        implements MainPresenter {

    private final String TAG = "myLogs." + getClass().getSimpleName();
    private List<ModelCategory> modelCategoryList;
    private Context context;
    private MainView view = null;
    //Preventing multitouch of screen elements
    private boolean elementAlreadyChosen = false;

    //Observable (RX) to get a signal that Items was got by a rest request
    private Observable obs = new Observable() {
        @Override
        protected void subscribeActual(Observer observer) {
            Log.i(TAG, " Observable obs was called ");

            Handler refresh = new Handler(Looper.getMainLooper());
            refresh.post(new Runnable() {
                public void run() {
                    //Stop spinner
                    view.stopProgressBar();

                    view.showInfo(modelCategoryList);
                }
            });
        }
    };

    //A Callback after finishing receiving json from a server
    private Callback<ResponseBody> responseBodyMainCallback = new Callback<ResponseBody>() {
        @Override
        public void onResponse(@NonNull Call<ResponseBody> call,
                               @NonNull Response<ResponseBody> response) {
            Log.i(TAG, " onCreate(): ApiRequestListener: gotten an answer ");

            String resp = "";
            try {
                resp = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (!resp.equals("")) {
                JSonParser jSonParser = new JSonParser();

                jSonParser.setObs(obs);
                jSonParser.execute(resp, "getArrayOfCategories");

                Log.i(TAG, " onCreate(): ApiRequestListener: finished ");
            } else {
                connectionErrorInformator();
            }
        }

        @Override
        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            Log.i(TAG, " the response is onFailure() method, error is: " + t.toString());
            connectionErrorInformator();
        }
    };

    private void connectionErrorInformator() {
        Toast.makeText(context, "A connection cannot be established. Try again later..."
                , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);

        //Destroyer for all global Fields
        modelCategoryList = null;
        context = null;
        view = null;
        obs = null;
        responseBodyMainCallback = null;
    }

    //Constructor(initializer)
    MainPresenterImpl(Context context) {
        this.modelCategoryList = DataKeeper.getInstance().getModelCategory();
        this.context = context;
    }

    //Getting restful info from a server to fill on a view
    private void startRestful() {
        view.startProgressBar();
        modelCategoryList.clear();

        //Refresh the RV through the Adapter with new data
        if (view.getAdapter() != null) {
            synchronized (view.getAdapter()) {
                view.getAdapter().notifyDataSetChanged();
            }
        }

        RestfulManager restfulManager = new RestfulManagerCategories();
        restfulManager.execute(0, responseBodyMainCallback);
    }

    //Treatment for the Catalogue items on the MiddleView
    @Override
    public void itemClicked(RecyclerView recyclerView, int position, final View v) {
        //Making view unclicked to prevent double-clicking
        v.findViewById(R.id.simple_item_view_root_layout).setClickable(false);

        if (!elementAlreadyChosen) {
            //Preventing multitouch of screen elements
            elementAlreadyChosen = true;

            YoYo.with(Techniques.RubberBand)
                    .duration(700)
                    .repeat(0)
                    .playOn(v.findViewById(R.id.simple_item_view_root_layout));

            if (DataKeeper.isOnline(context)) {
                final Intent intent = new Intent(v.getContext(), MiddleActivity.class);
                intent.putExtra("ItemPosition", position);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //        intent.putExtra("WhoSentIntent", getClass().getSimpleName());
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after XXXms
                        context.startActivity(intent);
                        elementAlreadyChosen = false;
                        v.findViewById(R.id.simple_item_view_root_layout).setClickable(true);
                    }
                }, 500);
            } else {
                elementAlreadyChosen = false;
                v.findViewById(R.id.simple_item_view_root_layout).setClickable(true);
            }
        }
    }

    //Refreshing of the Catalogue items on the MiddleView
    @Override
    public void refreshItemsOnView() {
        if (view == null) {
            if (isViewAttached()) {
                view = getView();
            } else {
                Log.i(TAG, " a View is not attached to the Presenter ");
            }
        }

        if (isViewAttached()) {
            if (DataKeeper.isOnline(context)) {
                view.changeBackgroud(true);
                startRestful();
                //showInfo will begin from a callback
            } else {
                modelCategoryList.clear();
                view.changeBackgroud(false);
                view.showInfo(modelCategoryList);
            }
        }
    }
}