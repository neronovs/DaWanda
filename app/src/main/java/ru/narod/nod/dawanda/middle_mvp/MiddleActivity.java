package ru.narod.nod.dawanda.middle_mvp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.List;

import ru.narod.nod.dawanda.Model.DataKeeper;
import ru.narod.nod.dawanda.Model.ModelProductListOfCategory.ModelProductListOfCategory;
import ru.narod.nod.dawanda.R;
import ru.narod.nod.dawanda.ReciclerViewPack.ItemClickSupport;
import ru.narod.nod.dawanda.ReciclerViewPack.RVAdapter_ModelProductListOfCategory;
import ru.narod.nod.dawanda.databinding.ActivityMainBinding;
import ru.narod.nod.dawanda.main_mvp.MainActivity;
import ru.narod.nod.dawanda.main_mvp.MainView;

public class MiddleActivity
        extends MvpActivity<MiddleView, MiddlePresenter>
        implements MiddleView {

    private final String TAG = "myLogs." + getClass().getSimpleName();
    private ActivityMainBinding activityMainBinding;
    private RecyclerView rv;
    private RVAdapter_ModelProductListOfCategory adapter;

    public RVAdapter_ModelProductListOfCategory getAdapter() {
        return adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        Intent intent = getIntent();
        int itemPosition = intent.getIntExtra("ItemPosition", 0);

        //RecyclerView is the place where fragments will be placed
        rv = activityMainBinding.rv;
        rv.setHasFixedSize(true);
        //Initing LinearLayoutManager to manage items in the RecyclerView
        rv.setLayoutManager(new LinearLayoutManager(this));

        //Setting up the onclick listener for items of the RecyclerView
        ItemClickSupport.addTo(rv).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        presenter.itemClicked(recyclerView, position, v);
                    }
                }
        );

        if (presenter != null) {
            presenter.setItemPosition(itemPosition);
            presenter.refreshItemsOnView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //Destroyer for all global Fields
        activityMainBinding = null;
        rv = null;
        adapter = null;
    }

    //Init a MiddlePresenter
    @NonNull
    @Override
    public MiddlePresenter createPresenter() {
        return new MiddlePresenterImpl(getApplicationContext());
    }

    //region menu treatment
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu() was started in MiddleActivity");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_middle, menu);
        return true;
    }

    //Treating of the menu (action bar) selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected() was started in MiddleActivity");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.menu_back:
                presenter.setCurrentActivityAlive(false);
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion

    //Stop showing the progress bar when the start search process is stopped
    public void stopProgressBar() {
        activityMainBinding.progressBar.setVisibility(View.GONE);
    }

    //Start showing the progress bar
    public void startProgressBar() {
        activityMainBinding.progressBar.setVisibility(View.VISIBLE);
    }


    //Filling the result activity with simple views
    public void showInfo(List<ModelProductListOfCategory> modelProductListOfCategory) {
        if (modelProductListOfCategory.size() > 0) {
            //If it was earkier shown and now it is not needed than hide it
            activityMainBinding.simpleNoResult.setVisibility(View.INVISIBLE);

            if (adapter == null) {
                adapter = new RVAdapter_ModelProductListOfCategory(
                        this, modelProductListOfCategory);
                rv.setAdapter(adapter);
            }

            adapter.notifyDataSetChanged();

            Log.i(TAG, " showInfo(): getting items to the search_result_view ");

        } else {
            //Show info that there is nothing to show
            activityMainBinding.simpleNoResult.setVisibility(View.VISIBLE);
        }

        Log.d(TAG, " SearchresultController: showInfo() was finished");
    }

    @Override
    public RecyclerView getReciclerView() {
        return rv;
    }
}
