package ru.narod.nod.dawanda.main_mvp;

import android.support.v7.widget.RecyclerView;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import ru.narod.nod.dawanda.Model.ModelCategory;
import ru.narod.nod.dawanda.ReciclerViewPack.RVAdapter_ModelCategory;

public interface MainView
        extends MvpView {

    void startProgressBar();
    void stopProgressBar();
    void showInfo(List<ModelCategory> modelCategoryArrayList);
    RecyclerView getReciclerView();
    RVAdapter_ModelCategory getAdapter();
    void changeBackgroud(boolean isInternetOK);
}
