package ru.narod.nod.dawanda.detailed_mvp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.Map;

import ru.narod.nod.dawanda.Model.DataKeeper;
import ru.narod.nod.dawanda.R;
import ru.narod.nod.dawanda.databinding.ActivityDetailedItemBinding;
import ru.narod.nod.dawanda.main_mvp.MainActivity;


public class DetailedActivity
        extends MvpActivity<DetailedView, DetailedPresenter>
        implements DetailedView, View.OnClickListener {

    private final String TAG = "myLogs." + getClass().getSimpleName();
    private ActivityDetailedItemBinding activityDetailedItemBinding;
    private ImageView detailedImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityDetailedItemBinding = DataBindingUtil.setContentView(
                this, R.layout.activity_detailed_item);

        detailedImageView = activityDetailedItemBinding.detailedImageView;
        detailedImageView.setOnClickListener(this);

        Intent intent = getIntent();
        int position = intent.getIntExtra("ItemPosition", 0);

        presenter.refreshItemsOnView(position);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        //Destroyer for all global Fields
        activityDetailedItemBinding = null;
        detailedImageView = null;
    }

    //Init a DetailedPresenter
    @NonNull
    @Override
    public DetailedPresenter createPresenter() {
        return new DetailedPresenterImpl();
    }

    //region menu treatment
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu() was started in DetailedActivity");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detailed, menu);
        return true;
    }

    //Treating of the menu (action bar) selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected() was started in DetailedActivity");
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_home:
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            case R.id.menu_back:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion

    //Stop showing the progress bar when the start search process is stopped
    public void stopProgressBar() {
        activityDetailedItemBinding.detailedProgressBar.setVisibility(View.GONE);
    }

    //Start showing the progress bar
    public void startProgressBar() {
        activityDetailedItemBinding.detailedProgressBar.setVisibility(View.VISIBLE);
    }

    //Filling the result activity with simple views
    public void showInfo(Map itemInfo, Drawable drawable) {
        activityDetailedItemBinding.detailedTvTitle.setText(itemInfo.get("title").toString());
        String priceAndCurrency = itemInfo.get("currency").toString()
                + itemInfo.get("price").toString();
        activityDetailedItemBinding.detailedTvPrice.setText(priceAndCurrency);
        activityDetailedItemBinding.detailedHasDiscount.setVisibility(
                itemInfo.get("discount") == "1" ? View.VISIBLE : View.INVISIBLE);

//        Drawable drawable = DataKeeper.getInstance().getImageViewForMiddle().getDrawable();
        if (drawable != null) {
            ImageView iv = (ImageView) findViewById(R.id.detailed_imageView);
            iv.setImageDrawable(drawable);
        } else {
            activityDetailedItemBinding.detailedImageView
                    .setBackgroundResource(R.mipmap.no_pic_available_small);
        }

        Log.d(TAG, " SearchresultController: showInfo() was finished");
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.detailed_imageView)
            presenter.imageClicked(view);
    }
}
