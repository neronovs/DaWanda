package ru.narod.nod.dawanda.Model;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.narod.nod.dawanda.Model.ModelProductListOfCategory.ModelProductListOfCategory;

public class DataKeeper {

    private static final DataKeeper ourInstance = new DataKeeper();
    private final String BASE_URL = "https://de.dawanda.com/third-party-public/";

    //A temporary ImageView to use in the detailed activity
    private ImageView imageViewForMiddle;
    public ImageView getImageViewForMiddle() {
        return imageViewForMiddle;
    }
    public void setImageViewForMiddle(ImageView imageViewForMiddle) {
        this.imageViewForMiddle = imageViewForMiddle;
    }

    //Array list of items of the MainMVP
    private List<ModelCategory> modelCategory;
    public List<ModelCategory> getModelCategory() {
        return modelCategory;
    }

    //Array list of items of the MiddleMVP
    private List<ModelProductListOfCategory> modelProductListOfCategory;
    public List<ModelProductListOfCategory> getModelProductListOfCategory() {
        return modelProductListOfCategory;
    }

    public static DataKeeper getInstance() {
        return ourInstance;
    }

    private DataKeeper() {
        modelCategory = new ArrayList<>();
        modelProductListOfCategory = new ArrayList<>();
    }

    //The base url setter/getter
    public String getBASE_URL() {
        return BASE_URL;
    }

    //The method of checking the Internet connection
    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        boolean isInternet = netInfo != null && netInfo.isConnectedOrConnecting();

        if (!isInternet)
            Toast.makeText(context, "The Internet connection is lost", Toast.LENGTH_SHORT).show();

        return isInternet;
    }
}
