package ru.narod.nod.dawanda.main_mvp;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

/**
 * Created by otc on 08.09.2017.
 */

public interface MainPresenter
        extends MvpPresenter<MainView> {

    void itemClicked(RecyclerView recyclerView, int position, View v);
    void refreshItemsOnView();

}
