
package ru.narod.nod.dawanda.Model.ModelProductListOfCategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shop {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("subdomain")
    @Expose
    private String subdomain;
    @SerializedName("holiday_from")
    @Expose
    private Object holidayFrom;
    @SerializedName("holiday_to")
    @Expose
    private Object holidayTo;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("auto_confirm")
    @Expose
    private Boolean autoConfirm;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(String subdomain) {
        this.subdomain = subdomain;
    }

    public Object getHolidayFrom() {
        return holidayFrom;
    }

    public void setHolidayFrom(Object holidayFrom) {
        this.holidayFrom = holidayFrom;
    }

    public Object getHolidayTo() {
        return holidayTo;
    }

    public void setHolidayTo(Object holidayTo) {
        this.holidayTo = holidayTo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getAutoConfirm() {
        return autoConfirm;
    }

    public void setAutoConfirm(Boolean autoConfirm) {
        this.autoConfirm = autoConfirm;
    }

}
