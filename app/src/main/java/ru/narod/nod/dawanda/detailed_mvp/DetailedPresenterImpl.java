package ru.narod.nod.dawanda.detailed_mvp;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.narod.nod.dawanda.Model.DataKeeper;
import ru.narod.nod.dawanda.Model.ModelProductListOfCategory.ModelProductListOfCategory;
import ru.narod.nod.dawanda.R;
import ru.narod.nod.dawanda.RestManager.JSonParser;
import ru.narod.nod.dawanda.RestManager.RestfulManager;
import ru.narod.nod.dawanda.RestManager.RestfulManagerItems;
import ru.narod.nod.dawanda.detailed_mvp.DetailedActivity;
import ru.narod.nod.dawanda.detailed_mvp.DetailedPresenter;
import ru.narod.nod.dawanda.detailed_mvp.DetailedView;


class DetailedPresenterImpl
        extends MvpBasePresenter<DetailedView>
        implements DetailedPresenter {

    private final String TAG = "myLogs." + getClass().getSimpleName();
    private DetailedView view = null;

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);

        //Destroyer for all global Fields
        view = null;
        DataKeeper.getInstance().setImageViewForMiddle(null);
    }

    //Constructor(initializer)
    DetailedPresenterImpl() {
    }

    //Treatment for the Catalogue items on the DetailedView
    @Override
    public void imageClicked(View v) {
        YoYo.with(Techniques.Shake)
                .duration(700)
                .repeat(0)
                .playOn(v.findViewById(R.id.detailed_imageView));
    }

    public void refreshItemsOnView(int position) {
        ModelProductListOfCategory item =
                DataKeeper.getInstance().getModelProductListOfCategory().get(position);

        Map<String, String> itemInfo = new HashMap<>();

        itemInfo.put("title", item.getTitle());
        itemInfo.put("currency", item.getPrice().getSymbol());
        double price = (double) item.getPrice().getCents() / 100;
        itemInfo.put("price", String.valueOf(price));
        if (item.getDiscounted() == null)
            itemInfo.put("discount", "0");
        else
            itemInfo.put("discount", "1");

        itemInfo.put("pic_url", "http:" + item.getDefaultImage().getBig());

        if (isViewAttached()) {
            view = getView();
            Drawable drawable = DataKeeper.getInstance().getImageViewForMiddle().getDrawable();
            view.showInfo(itemInfo, drawable);
        } else {
            Log.i(TAG, " a View is not attached to the Presenter ");
        }
    }
}
