package ru.narod.nod.dawanda.ReciclerViewPack;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.narod.nod.dawanda.Model.ModelCategory;
import ru.narod.nod.dawanda.R;
import ru.narod.nod.dawanda.databinding.SimpleItemViewBinding;

public class RVAdapter_ModelCategory
        extends RecyclerView.Adapter<RVAdapter_ModelCategory.ItemViewHolder> {

    private List<ModelCategory> model;
    private final String TAG = "myLogs." + getClass().getSimpleName();
    private Context context;
    private SimpleItemViewBinding binding;

    public RVAdapter_ModelCategory(Context context, List<ModelCategory> model) {
        this.model = model;
        this.context = context;
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {
        ModelCategory modelCategory;

        TextView simple_tvTitle;
        ImageView simple_imageThumbnail;
        com.wang.avi.AVLoadingIndicatorView progressBar;
        private final String TAG = "myLogs." + getClass().getSimpleName();

        ItemViewHolder(final View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            simple_tvTitle = binding.simpleTvTitle;
            simple_imageThumbnail = binding.simpleImageThumbnail;
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.simple_item_view, viewGroup, false);
        ItemViewHolder ivh = new ItemViewHolder(v);
        return ivh;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder itemViewHolder, final int position) {
        itemViewHolder.modelCategory = model.get(position);
        itemViewHolder.simple_tvTitle.setText(model.get(position).getName());

//        itemViewHolder.progressBar.setVisibility(View.INVISIBLE);

        String pic_url = "http:" + model.get(position).getImageUrl().trim();

        if (!pic_url.equals("")) {
            Picasso.with(context)
                    .load(pic_url)
                    .into(itemViewHolder.simple_imageThumbnail);
        } else {
            Log.i("Url 4 Picasso is empty!", String.valueOf(model.get(position).getId()));
        }
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}