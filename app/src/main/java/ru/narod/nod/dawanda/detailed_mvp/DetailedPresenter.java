package ru.narod.nod.dawanda.detailed_mvp;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ru.narod.nod.dawanda.middle_mvp.MiddleView;


interface DetailedPresenter
        extends MvpPresenter<DetailedView> {

    void imageClicked(View v);
    void refreshItemsOnView(int position);

}
