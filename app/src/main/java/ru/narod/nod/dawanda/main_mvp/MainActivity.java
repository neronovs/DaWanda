package ru.narod.nod.dawanda.main_mvp;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.List;

import ru.narod.nod.dawanda.Model.DataKeeper;
import ru.narod.nod.dawanda.Model.ModelCategory;
import ru.narod.nod.dawanda.R;
import ru.narod.nod.dawanda.databinding.ActivityMainBinding;
import ru.narod.nod.dawanda.ReciclerViewPack.ItemClickSupport;
import ru.narod.nod.dawanda.ReciclerViewPack.RVAdapter_ModelCategory;

public class MainActivity
        extends MvpActivity<MainView, MainPresenter>
        implements MainView {

    private final String TAG = "myLogs." + getClass().getSimpleName();
    private ActivityMainBinding activityMainBinding;
    private RecyclerView rv;

    private RVAdapter_ModelCategory adapter;
    public RVAdapter_ModelCategory getAdapter() {
        return adapter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //Destroyer for all global Fields
        activityMainBinding = null;
        rv = null;
        adapter = null;
    }

    @Override
    public void changeBackgroud(boolean isInternetOK) {
        //Macking background relevant to the inet connection state
        if (isInternetOK)
            activityMainBinding.mainBackgroundLayout.setBackgroundResource(R.mipmap.background);
        else
            activityMainBinding.mainBackgroundLayout.setBackgroundResource(R.mipmap
                    .no_internet_connection);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Catalog.Dawanda");

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);


        //RecyclerView is the place where fragments will be placed
        rv = activityMainBinding.rv;
        rv.setHasFixedSize(true);
        //Initing LinearLayoutManager to manage items in the RecyclerView
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        //Setting up the onclick listener for items of the RecyclerView
        ItemClickSupport.addTo(rv).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        presenter.itemClicked(recyclerView, position, v);
                    }
                }
        );

        presenter.refreshItemsOnView();

    }


    //Init a MiddlePresenter
    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenterImpl(getApplicationContext());
    }

    //region menu treatment
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu() was started in MiddleActivity");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Treating of the menu (action bar) selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected() was started in MiddleActivity");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.menu_search:
                presenter.refreshItemsOnView();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion

    //Stop showing the progress bar when the start search process is stopped
    public void stopProgressBar() {
        activityMainBinding.progressBar.setVisibility(View.GONE);
    }

    //Start showing the progress bar
    public void startProgressBar() {
        activityMainBinding.progressBar.setVisibility(View.VISIBLE);
    }


    //Filling the result activity with simple views
    public void showInfo(List<ModelCategory> modelCategoryArrayList) {
        activityMainBinding.rv.removeAllViews();
        if (modelCategoryArrayList.size() > 0) {
            //If it was earkier shown and now it is not needed than hide it
            activityMainBinding.simpleNoResult.setVisibility(View.INVISIBLE);

            if (adapter == null) {
                adapter = new RVAdapter_ModelCategory(this, modelCategoryArrayList);
                rv.setAdapter(adapter);
            }

            adapter.notifyDataSetChanged();

            Log.i(TAG, " showInfo(): getting items to the search_result_view ");

        } else {
            //Show info that there is nothing to show
            if (DataKeeper.isOnline(this))
                activityMainBinding.simpleNoResult.setVisibility(View.VISIBLE);
        }

        Log.d(TAG, " SearchresultController: showInfo() was finished");
    }

    @Override
    public RecyclerView getReciclerView() {
        return rv;
    }
}
