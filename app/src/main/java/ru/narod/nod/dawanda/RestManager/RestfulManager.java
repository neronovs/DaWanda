package ru.narod.nod.dawanda.RestManager;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.narod.nod.dawanda.Model.DataKeeper;


interface CategoriesClientInterface {
    @GET("categories.json")
    Call<ResponseBody> daWandaSearch();
}

interface ItemsClientInterface {
    @GET("categories/{id}.json")
    Call<ResponseBody> daWandaSearch(@Path("id") int id);
}

public abstract class RestfulManager
        extends AsyncTask<Object, Object, Void> {

    protected final String TAG = "myLogs." + getClass().getSimpleName();
    private Gson gson;

    Retrofit prepareRestAPI() {
        gson = new GsonBuilder().create();
        String url_base = DataKeeper.getInstance().getBASE_URL();

        //Change timeout to 15 secs
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .baseUrl(url_base)
                .build();
    }

    abstract void startRestAPI(int id, Callback<ResponseBody> apiRequestListener);
}