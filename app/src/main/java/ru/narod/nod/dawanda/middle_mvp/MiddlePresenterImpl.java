package ru.narod.nod.dawanda.middle_mvp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.io.IOException;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.narod.nod.dawanda.Model.DataKeeper;
import ru.narod.nod.dawanda.Model.ModelProductListOfCategory.ModelProductListOfCategory;
import ru.narod.nod.dawanda.R;
import ru.narod.nod.dawanda.RestManager.JSonParser;
import ru.narod.nod.dawanda.RestManager.RestfulManager;
import ru.narod.nod.dawanda.RestManager.RestfulManagerItems;
import ru.narod.nod.dawanda.detailed_mvp.DetailedActivity;


class MiddlePresenterImpl
        extends MvpBasePresenter<MiddleView>
        implements MiddlePresenter {

    private final String TAG = "myLogs." + getClass().getSimpleName();
    private List<ModelProductListOfCategory> modelProductListOfCategory;
    private Context context;
    private MiddleView view = null;

    private int itemPosition;


    //If has been pressed button "back" before started a detailed view, than it mustn't start
    private boolean currentActivityAlive = true;
    public void setCurrentActivityAlive(boolean currentActivityAlive) {
        this.currentActivityAlive = currentActivityAlive;
    }

    public void setItemPosition(int itemPosition) {
        this.itemPosition = itemPosition;
    }

    //Preventing multitouch of screen elements
    private boolean elementAlreadyChosen = false;

    //Observable (RX) to get a signal that Items was got by a rest request
    private Observable obs = new Observable() {
        @Override
        protected void subscribeActual(Observer observer) {
            Log.i(TAG, " Observable obs was called ");

            Handler refresh = new Handler(Looper.getMainLooper());
            refresh.post(new Runnable() {
                public void run() {
                    //Stop spinner
                    view.stopProgressBar();

                    view.showInfo(modelProductListOfCategory);
                }
            });
        }
    };

    //A Callback after finishing receiving json from a server
    private Callback<ResponseBody> responseBodyMiddleCallback = new Callback<ResponseBody>() {
        @Override
        public void onResponse(@NonNull Call<ResponseBody> call,
                               @NonNull Response<ResponseBody> response) {
            Log.i(TAG, " onCreate(): ApiRequestListener: gotten an answer ");

            String resp = "";
            try {
                resp = response.body().string();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }

            if (!resp.equals("")) {
                final JSonParser jSonParser = new JSonParser();

                jSonParser.setObs(obs);
                jSonParser.execute(resp, "getArrayOfItems");

                Log.i(TAG, " responseBodyMiddleCallback: finished ");
            } else {
                connectionErrorInformator();
            }
        }

        @Override
        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            Log.i(TAG, " the response is onFailure() method, error is: " + t.toString());
            connectionErrorInformator();
        }
    };

    private void connectionErrorInformator() {
        Toast.makeText(context, "A connection cannot be established. Try again later..."
                , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);

        //Destroyer for all global Fields
        modelProductListOfCategory = null;
        context = null;
        view = null;
        obs = null;
        responseBodyMiddleCallback = null;
        DataKeeper.getInstance().setImageViewForMiddle(null);
    }

    //Constructor(initializer)
    MiddlePresenterImpl(Context context) {
        this.modelProductListOfCategory = DataKeeper.getInstance()
                .getModelProductListOfCategory();
        this.context = context;
    }

    //Getting restful info from a server to fill on a view
    private void startRestful() {
        view.startProgressBar();
        modelProductListOfCategory.clear();

        //Refresh the RV through the Adapter with new data
        if (view.getAdapter() != null) {
            synchronized (view.getAdapter()) {
                view.getAdapter().notifyDataSetChanged();
            }
        }

        RestfulManager restfulManager = new RestfulManagerItems();
        restfulManager.execute(itemPosition, responseBodyMiddleCallback);
    }

    //Treatment for the Catalogue items on the MiddleView
    @Override
    public void itemClicked(RecyclerView recyclerView, int position, final View v) {
        //Making view unclicked to prevent double-clicking
        v.findViewById(R.id.middle_item_view_root_layout).setClickable(false);

        DataKeeper.isOnline(context);

        if (!elementAlreadyChosen) {
            //Preventing multitouch of screen elements
            elementAlreadyChosen = true;
            YoYo.with(Techniques.Wave)
                    .duration(700)
                    .repeat(0)
                    .playOn(v.findViewById(R.id.middle_item_view_root_layout));

            //Saving the ImageView in the DataKeeper to use in the detailed activity
            DataKeeper.getInstance().setImageViewForMiddle(
                    (ImageView) v.findViewById(R.id.middle_image));

            final Intent intent = new Intent(v.getContext(), DetailedActivity.class);

            intent.putExtra("ItemPosition", position);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //intent.putExtra("WhoSentIntent", getClass().getSimpleName());

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after XXXms
                    if (currentActivityAlive) {
                        context.startActivity(intent);
                        elementAlreadyChosen = false;
                        v.findViewById(R.id.middle_item_view_root_layout).setClickable(true);
                    }
                }
            }, 600);
        }
    }

    //Refreshing of the Catalogue items on the MiddleView
    @Override
    public void refreshItemsOnView() {
        if (view == null) {
            if (isViewAttached()) {
                view = getView();
                startRestful();

            } else {
                Log.i(TAG, " a View is not attached to the Presenter ");
            }
        } else {
            startRestful();
        }
    }
}