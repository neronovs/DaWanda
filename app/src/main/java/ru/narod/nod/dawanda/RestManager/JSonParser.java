package ru.narod.nod.dawanda.RestManager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import io.reactivex.Observable;
import ru.narod.nod.dawanda.Model.DataKeeper;
import ru.narod.nod.dawanda.Model.ModelCategory;
import ru.narod.nod.dawanda.Model.ModelProductListOfCategory.ModelProductListOfCategory;


public class JSonParser extends AsyncTask<String, String, Void> {

    private final String TAG = "myLogs." + getClass().getSimpleName();

    public void setObs(Observable obs) {
        this.obs = obs;
    }
    private Observable obs = null;

    public JSonParser() {
        Log.i(TAG, " JSonParser constructor: is done!");
    }

    @Override
    protected Void doInBackground(String... strings) {
        String jsonString = strings[0];
        String method = strings[1];

        if (method.equals("getArrayOfCategories"))
            getArrayOfCategories(jsonString, obs);
        else
            getArrayOfItems(jsonString, obs);

        obs = null;
        return null;
    }

    //Taking an Array of Category
    private List<ModelCategory> getArrayOfCategories(String jsonString, Observable obs) {
        JSONArray jsonArray = null;

        try {
            jsonArray = new JSONArray(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        assert jsonArray != null;

        List<ModelCategory> modelCategoryList = DataKeeper.getInstance().getModelCategory();
        modelCategoryList.clear();

        for (int i = 0; i < jsonArray.length(); i++) {

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();

            try {
                modelCategoryList.add(gson.fromJson(jsonArray.getString(i), ModelCategory.class));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (obs != null)
            obs.subscribe();

        return modelCategoryList; //for testing
    }

    //Taking an Array of Items
    private List<ModelProductListOfCategory> getArrayOfItems(String jsonString, Observable obs) {
        JSONArray jsonArray = null;

        try {
            jsonArray = new JSONArray(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        assert jsonArray != null;

        List<ModelProductListOfCategory> modelItemsList = DataKeeper.getInstance()
                .getModelProductListOfCategory();
        modelItemsList.clear();

        for (int i = 0; i < jsonArray.length(); i++) {

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();

            try {
                modelItemsList.add(gson.fromJson(jsonArray.getString(i),
                        ModelProductListOfCategory.class));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (obs != null)
            obs.subscribe();

        return modelItemsList; //for testing
    }
}