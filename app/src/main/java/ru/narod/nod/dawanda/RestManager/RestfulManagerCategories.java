package ru.narod.nod.dawanda.RestManager;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class RestfulManagerCategories
        extends RestfulManager {

    void startRestAPI(int id, Callback<ResponseBody> apiRequestListener) {
        Retrofit retrofit = super.prepareRestAPI();

        Call<ResponseBody> call = null;

//        if (id != null) {
            call = retrofit.create(CategoriesClientInterface.class)
                    .daWandaSearch();
//        }

        if (call != null) {
            call.clone().enqueue(apiRequestListener);
        }
    }

    @Override
    protected Void doInBackground(Object[] params) {
        int id = (int) params[0];
        Callback<ResponseBody> apiRequestListener = null;

        if (params[1] instanceof Callback) {
            apiRequestListener = (Callback<ResponseBody>) params[1];
        }

        startRestAPI(id, apiRequestListener);

        return null;
    }
}
