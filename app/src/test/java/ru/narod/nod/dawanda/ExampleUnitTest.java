package ru.narod.nod.dawanda;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;

import ru.narod.nod.dawanda.Model.DataKeeper;
import ru.narod.nod.dawanda.main_mvp.MainActivity;
import ru.narod.nod.dawanda.main_mvp.MainPresenter;
import ru.narod.nod.dawanda.main_mvp.MainView;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 * https://stfalcon.com/ru/blog/post/simple-unit-tests-for-android
 */

@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void addition_isIncorrect() throws Exception {
        assertNotEquals("Numbers aren't equal", 5, 2 + 4);
    }

    @Test(expected = NullPointerException.class)
    public void nullStringTest() {
        String s = null;
        assertTrue(s.isEmpty());
    }

    @Test(timeout = 22)
    public void timeoutTest() {
        String s = "";
        assertTrue(s.isEmpty());
    }

    //http://hamcrest.org/JavaHamcrest/javadoc/1.3/org/hamcrest/CoreMatchers.html
    @Test
    public void matchersTest() {
        int x = 3;
        List<Integer> list = new ArrayList<>();
        list.add(x);
        list.add(7);
        assertThat(x, is(3));
        assertThat(x, is(not(4)));
        assertThat(list, hasItem(3));
    }

    @Mock
    Context mockContext;
    @Mock
    ArrayList mockedList = mock(ArrayList.class);

    @Test
    public void mockingTest() {
        when(mockContext.getPackageName())
                .thenReturn("Fake name");

        assertThat(mockContext.getPackageName(), is ("Fake name"));
        assertTrue(mockedList.size() == 0);
        assertThat(mockedList.size(), is (0));
    }

    @Test
    public void verifyTest() {
        mockedList.add("12");
        mockedList.clear();

        verify(mockedList).add("12");
        verify(mockedList).clear();
    }

    List spyList = spy(new LinkedList());

    @Test
    public void spyTest() {
        spyList.add("12");
        spyList.clear();

        verify(spyList).add("12");
        verify(spyList).clear();
    }
}